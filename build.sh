#!/bin/bash

# The first sector
nasm bootloader1.asm -f bin -o os.img -w+orphan-labels

# Add signature
PAD=$((510 - `stat -c%s os.img`))
dd if=/dev/zero bs=$PAD count=1 >> os.img
echo -en "\x55\xAA" >> os.img

# Second stage
nasm bootloader2.asm -f bin -o os2.img -w+orphan-labels
PAD=$((512 - (`stat -c%s os2.img` % 512)))
dd if=os2.img >> os.img
dd if=/dev/zero bs=$PAD count=1 >> os.img
rm os2.img

# Do some "linking"
ADDR="0x`grep set_gdt map-bootloader1.map | sed 's/  */ /g' | cut -d\  -f3`"
ORIGIN="0x`head -n9 map-bootloader1.map | tail -1`"
OFFSET="$(($ADDR - $ORIGIN - 2))"

GDTR="0x`grep gdtr_32 map-bootloader2.map | sed 's/  */ /g' | cut -d\  -f3`"
GDTR="`printf '%X' $(($GDTR % 0x10000))`"

echo "`printf '%X' $OFFSET`: `printf '%X' $((0x$GDTR % 0x100))`" | xxd -r - os.img
((OFFSET++))
echo "`printf '%X' $OFFSET`: `printf '%X' $((0x$GDTR / 0x100))`" | xxd -r - os.img



# If you wish, modify this build script to append your kernel entry point
