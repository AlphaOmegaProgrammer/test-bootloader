[map all map-bootloader1.map]

USE16
ORG 0x7C00

segment .text
	cli

	; Print booting message
	mov ax, 0x1300
	mov bx, 0x000F
	mov cx, 6
	mov bp, BOOTING

	int 0x10

	; Enable A20
	mov ax, 0x2401
	mov bx, 1
	int 0x15

	; Get memory map
	mov ax, 0x7000
	mov es, ax
	mov eax, 0xE820
	xor ebx, ebx
	mov ecx, 24
	mov edx, 0x0534D4150
	mov di, 0xE000
	int 0x15

	jc done_e820

read_e820:
	cmp eax, edx
	jne done_e820

	cmp ebx, 0
	je done_e820

	mov dword [es:di + 20], 1

	mov eax, 0xE820
	mov ecx, 24
	add di, 32
	int 0x15

	jmp read_e820

done_e820:

	; Read more sectors from drive A:
	mov ax, 0x0220
	mov cx, 0x0002
	xor dx, dx

	mov bx, 0x7000
	mov es, bx
	mov bx, 0x8000

	int 0x13

	; Load the 32-bit GDT
	lgdt [es:0]
set_gdt: ; This is used for "linking" in the build script. DO NOT REMOVE

	; Enable Protected Mode
	mov eax, 0x40050031
	mov cr0, eax

	; Now we enter protected mode
	jmp 8:start_32

USE32
start_32:
	; Now that we're in protected mode, we're able to jump to stage 2
	jmp 0x00078000

segment .data
	BOOTING db "16-bit"
