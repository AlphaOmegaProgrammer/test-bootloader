[map all map-bootloader2.map]

USE32
ORG 0x00078000

segment .text
	; Set segment registers
	mov eax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov fs, ax
	mov gs, ax

	; Set the text attributes for all characters to white on black and clear screen
	mov edi, 0x000B8000
	mov eax, 0x0F20
	mov ecx, 0x4000
	rep stosw

	; "32-bit"
	mov dword [0x000B8000], 0x0F320F33
	mov dword [0x000B8004], 0x0F620F2D
	mov dword [0x000B8008], 0x0F740F69



	; Check if CPU is 64-bit
	mov eax, 0x80000000
	cpuid
	cmp eax, 0x80000001
	jl cpu_not_64_bit

	mov eax, 0x80000001
	cpuid
	test edx, 1 << 29
	jz cpu_not_64_bit



	; Clear all memory below 0x00078000
	mov edi, 0x00000000
	xor eax, eax
	mov ecx, 0x1E000
	rep stosd



	; Set up the GDT at 0x0
	mov dword [0x0C], 0x00209C00	; Code segment
	mov dword [0x14], 0x00009000	; Data segment

	; Load the GDT
	lgdt [gdtr_64]



	; Set up temporary paging for the first 2MB
	; Set up PML4
	mov dword [0x00077000], 0x00076007

	; Set up PDPE
	mov dword [0x00076000], 0x00075007

	; Set up PDE
	mov dword [0x00075000], 0x0000008F



	; Point cr3 to the level 4 page map (PML4)
	mov eax, 0x00077008
	mov cr3, eax

	; Enable extended properties (PAE)
	mov eax, 0x000000B0
	mov cr4, eax

	; Set EFER
	mov ecx, 0xC0000080
	rdmsr
	or eax, 0x00000101
	wrmsr

	; Enable paging (active long mode)
	mov eax, 0xC0050031
	mov cr0, eax

	; Now we enter long mode
	jmp 8:start_64



; If the CPU isn't 64-bit, say something
cpu_not_64_bit:
	mov dword [0x000B8000], 0x0F6F0F4E
	mov dword [0x000B8004], 0x0F200F74
	mov dword [0x000B8008], 0x0F340F36
	mov dword [0x000B800C], 0x0F620F2D
	mov dword [0x000B8010], 0x0F740F69

; It just crashes here... I don't think I set up protected mode all the way lol
cpu_not_64_bit_hang:
	hlt
	jmp cpu_not_64_bit_hang



USE64
start_64:
	; "64-bit"
	mov dword [0x000B8000], 0x0F340F36

	; Set rsp and rbp to safe values with the current memory layout
	mov rsp, 0x00075000
	mov rbp, rsp

	; And then jump to the kernel entry point
	jmp 0x0000000000078200


segment .data
gdtr_32:
	dw 0xFFFF
	dq gdt_32

gdt_32:
	dq 0
	dq 0x00CF9A000000FFFF	; Code segment
	dq 0x00CF92000000FFFF	; Data segment


gdtr_64:
	dw 0xFFFF
	dq 0x0000000000000000
